# CppPostgresTest

Simple project to learn C++ with Postgresql API.



## Environment

### Arch Linux

Packages used:
1. [postgresql](https://archlinux.org/packages/extra/x86_64/postgresql/)
2. [qtcreator](https://archlinux.org/packages/extra/x86_64/qtcreator/)
3. [gcc](https://archlinux.org/packages/core/x86_64/gcc/)
4. [gdb](https://archlinux.org/packages/extra/x86_64/gdb/)
5. [libpqxx](https://archlinux.org/packages/extra/x86_64/libpqxx/)
6. [dbeaver](https://archlinux.org/packages/community/x86_64/dbeaver/)

## Getting started

### Utils directory contains script to create environment
1. [dependencies.sh](utils/dependencies.sh) - installs all dependencies needed
2. [CreateUserTable.sql](utils/CreateUserTable.sql) - create the table needed for the CPP program
3. [codfisc.py](utils/codfisc.py) - simple python script to validate italian fiscal codes (Codici fiscali)

### Configuration files expected 

1. ConnectionString.cfg -> file containing string for Database
2. FiscalCodes.txt -> file containing Fiscal Codes (Codici Fiscali) to create the user class
