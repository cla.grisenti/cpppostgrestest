#pragma once

#include "../commons/Consts.h"
#include "../commons/StopWatch.h"
#include "../commons/Utils.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace CppPostgresqlTest::ReadFromFile {

void ReadFileLinesChunks(std::vector<std::string> &res,
                         std::ifstream &input,
                         size_t chunk_size = __CHUNK_SIZE_CONST__)
{
    ExecuteLogBuild(CppPostgresTest::StopWatch::StopWatch stop = __StopWatchAutoConstructor__);

    res.reserve(chunk_size);

    std::string s{};
    size_t count{0};

    while (count < chunk_size && std::getline(input, s)) {
        res.emplace_back(std::move(s));

        ++count;
    }

    ExecuteLogBuild(std::cout << "Read " << count
                              << " lines from file - Chunk size : " << chunk_size << "\n");
}

} // namespace CppPostgresqlTest::ReadFromFile
