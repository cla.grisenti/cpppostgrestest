#include "commons/Consts.h"
#include "commons/StopWatch.h"
#include "commons/Utils.h"
#include "user/User_InserDB.h"
#include <iostream>

int main()
{
    ExecuteLogBuild(CppPostgresTest::StopWatch::StopWatch stop = __StopWatchAutoConstructor__);
    CppPostgresqlTest::User::InsertDB::BatchInsertUsers();
}
