#include "../commons/StopWatch.h"
#include "../commons/Utils.h"
#include "../connectionString/ConnectionStringHelper.h"
#include <future>
#include <pqxx/pqxx>

#pragma once

namespace CppPostgresqlTest::DataBaseWorker {

void CreateJob(std::string &connectionstring, std::string &sql)
{
    ExecuteLogBuild(CppPostgresTest::StopWatch::StopWatch stop = __StopWatchAutoConstructor__);

    pqxx::connection connection(connectionstring);

    ExecuteLogBuild(std::cout << "Opened database successfully: " << connection.dbname()
                              << std::endl);

    pqxx::work work(connection);

    work.exec(sql);
    work.commit();
}

class DataBaseWorker
{
public:
    DataBaseWorker(
        std::string connecString = CppPostgresqlTest::ConnectionString::GetConnectionString())
        : ConnectionString(connecString)
    {}

    void ExecuteQuery(std::string &sql) { AddQueryToPool(WorkPool, sql); }

    ~DataBaseWorker()
    {
        std::for_each(WorkPool.begin(), WorkPool.end(), [](std::future<void> &fut) -> void {
            fut.get();
        });
    }

private:
    void AddQueryToPool(std::vector<std::future<void>> &workPool, std::string &sql)
    {
        workPool.emplace_back(std::async(CreateJob, std::ref(ConnectionString), std::ref(sql)));
    }

    std::string ConnectionString;

    std::vector<std::future<void>> WorkPool;
};

} // namespace CppPostgresqlTest::DataBaseWorker
