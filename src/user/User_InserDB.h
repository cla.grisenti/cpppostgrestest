#include "../commons/Consts.h"
#include "../commons/StopWatch.h"
#include "../commons/Utils.h"
#include "../dataBaseWorker/DataBaseWorker.h"
#include "../readFromFile/ReadFromFile.h"
#include "../user/User.h"
#include <iostream>
#include <pqxx/pqxx>
#include <vector>

namespace CppPostgresqlTest::User::InsertDB {

void ExecuteQuery(CppPostgresqlTest::DataBaseWorker::DataBaseWorker &worker, std::string &sql)
{
    worker.ExecuteQuery(sql);
}

std::string DefaultInsertQuery()
{
    return std::string("INSERT INTO public.\"User\" (\"Name\",\"Surname\",\"FiscalCode\") VALUES ");
}

bool InsertUsers(const std::vector<User> &users, size_t chunk_size = __CHUNK_SIZE_CONST__)
{
    try {
        if (users.size() > 0) {
            std::string sql = DefaultInsertQuery();
            sql.reserve(std::max(chunk_size, users.size()) * (16 + 6));
            size_t count{0};

            CppPostgresqlTest::DataBaseWorker::DataBaseWorker worker;

            ExecuteLogBuild(
                CppPostgresTest::StopWatch::StopWatch stop = __StopWatchAutoConstructor__);

            std::string tmp;

            for (const User &user : users) {
                tmp = std::string(1, (' ' - ((' ' - ',')) * (count != 0 && count <= chunk_size)));

                tmp.append("('" + user.Name + "','" + user.Surname + "','" + user.FiscalCode
                           + "') ");

                sql.append(std::move(tmp));

                count++;

                if (count > chunk_size) {
                    CppPostgresqlTest::User::InsertDB::ExecuteQuery(worker, sql);
                    count = 0;
                    sql = DefaultInsertQuery();
                }
            }

            if (count != 0) {
                CppPostgresqlTest::User::InsertDB::ExecuteQuery(worker, sql);
            }
        }
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;

        return false;
    }

    return true;
}

bool BatchInsertUsers(const std::string filePath = __FilscalCodes__,
                      size_t chunk_size = __CHUNK_SIZE_CONST__)
{
    std::vector<CppPostgresqlTest::User::User> users;
    users.reserve(chunk_size);
    std::vector<std::string> codiciFiscali;
    codiciFiscali.reserve(chunk_size);

    std::ifstream fileCodiciFiscali{filePath};
    bool resInsert{fileCodiciFiscali.good()};

    if (fileCodiciFiscali.good()) {
        while (resInsert && !fileCodiciFiscali.eof()) {
            ExecuteLogBuild(
                CppPostgresTest::StopWatch::StopWatch stop = __StopWatchAutoConstructor__);

            CppPostgresqlTest::ReadFromFile::ReadFileLinesChunks(codiciFiscali,
                                                                 fileCodiciFiscali,
                                                                 chunk_size);

            for (const std::string &str : codiciFiscali) {
                users.push_back({str});
            }

            resInsert = CppPostgresqlTest::User::InsertDB::InsertUsers(users);

            codiciFiscali.clear();
            users.clear();
            users.reserve(chunk_size);
            codiciFiscali.reserve(chunk_size);
        }
    } else {
        // File with italian fiscal codes (Codici fiscali) expected
        std::cerr << "File" << filePath << "not found\n";
    }

    return resInsert;
}

} // namespace CppPostgresqlTest::User::InsertDB
