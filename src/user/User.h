#pragma once

#include<string>
namespace CppPostgresqlTest::User {

class User
{
public:
    User(const std::string &codFiscale) : FiscalCode{codFiscale}
    {
        // TODO ->  Fix stub creation for this entity
        //          Substring is used because italian fiscal code is expected
        //
        //          Italian Fiscal code (Codice Fiscale) includes in the first 6 characters
        //          letters from both name and surname
        Name = FiscalCode.substr(0, 3);
        Surname = FiscalCode.substr(3, 3);
    }

    std::string Name;
    std::string Surname;
    // Primary key of User
    std::string FiscalCode;
};

} // namespace CppPostgresqlTest::User
