#pragma once

#include "../commons/Consts.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace CppPostgresqlTest::ConnectionString {

std::string GetConnectionString(const char *file = __ConnectionString__)
{
    std::string res{};

    std::ifstream input{file};

    if (input.good()) {
        std::getline(input, res);
    } else {
        throw std::invalid_argument("ConnectionString.cfg file not found");
    }

    return res;
}

} // namespace CppPostgresqlTest::ConnectionString
