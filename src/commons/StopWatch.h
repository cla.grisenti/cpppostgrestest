#pragma once

#include <chrono>
#include <iostream>

namespace CppPostgresTest::StopWatch {

std::chrono::system_clock::time_point CurrentTimePoint()
{
    return std::chrono::system_clock::now();
}

class StopWatch
{
public:
    StopWatch(std::string file, std::string func, int line) : File(file), Func(func), Line(line)
    {
        SetStartTimePoint();
    }

    void SetStartTimePoint() { _startTimePoint = CurrentTimePoint(); }

    std::chrono::duration<double> GetElapsedTime() const
    {
        std::chrono::duration<double> duration = std::chrono::system_clock::now() - _startTimePoint;
        return duration;
    }

    std::chrono::milliseconds GetElapsedMilliSeconds()
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>(GetElapsedTime());
    }

    friend std::ostream &operator<<(std::ostream &stream, const StopWatch &watch);

    std::chrono::system_clock::time_point _startTimePoint;

    ~StopWatch()
    {
        std::cout << File << " - " << Func << " - " << Line << " : "
                  << GetElapsedMilliSeconds().count() << " ms\n";
    }

private:
    std::string File;
    std::string Func;
    int Line;
};

std::ostream &operator<<(std::ostream &stream, const StopWatch &watch)
{
    stream << std::chrono::duration_cast<std::chrono::milliseconds>(watch.GetElapsedTime()).count();

    return stream;
}

} // namespace CppPostgresTest::StopWatch

#define __StopWatchAutoConstructor__ \
    CppPostgresTest::StopWatch::StopWatch(__FILE__, __FUNCTION__, __LINE__)
