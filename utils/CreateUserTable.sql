-- public."User" definition

-- Drop table

-- DROP TABLE public."User";

CREATE TABLE public."User" (
	"Name" varchar NOT NULL,
	"Surname" varchar NOT NULL,
	"FiscalCode" bpchar(16) NOT NULL,
	CONSTRAINT user_pk PRIMARY KEY ("FiscalCode")
);
