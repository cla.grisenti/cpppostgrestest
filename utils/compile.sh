#!/bin/bash

COMPILER="g++"
STD="-std=c++17"
OPT="-O3"
LVLWAR="-Wall -Wextra"
#LVLWAR=""
LIBS="-lpqxx -lpq -pthread"
TOT="$STD $OPT $LVLWAR $LIBS"

$COMPILER -S $TOT $1 && $COMPILER $TOT $1 && time ./a.out
