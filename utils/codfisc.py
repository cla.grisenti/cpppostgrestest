from codicefiscale import codicefiscale
import json

fileRead = open("FilscalCodesNotValidated.txt", "r")
CodFisc = open("FiscalCodes.txt", "a")

count = 0

for x in fileRead:
    try:
        if codicefiscale.is_valid(x):
            CodFisc.write(x)
            count = count +1
    except Exception as e:
        print(e)

print(count)
